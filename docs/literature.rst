Literature
==========

Publications that describe the theory behind Smuthi:

[Egel 2018]
   `Amos Egel: "Accurate optical simulation of disordered scattering layers for light extractionfrom organic light emitting diodes", Dissertation, Karlsruhe (2018), DOI: 10.5445/IR/1000093961 <https://publikationen.bibliothek.kit.edu/1000093961/26467128>`_

[Egel and Lemmer 2014]
   `Amos Egel, Uli Lemmer: "Accurate optical simulation of disordered scattering layers for light extractionfrom organic light emitting diodes", Karlsruhe, 2018, DOI: 10.5445/IR/1000093961 <https://publikationen.bibliothek.kit.edu/1000093961/26467128>`_

[Egel et al. 2016a]
   `Amos Egel, Siegfried W. Kettlitz, Uli Lemmer. "Efficient evaluation of Sommerfeld integrals for the optical simulation of many scattering particles in planarly layered media." JOSA A 33.4 (2016): 698-706. <https://www.osapublishing.org/josaa/abstract.cfm?uri=josaa-33-4-698>`_

[Egel et al. 2016b]
   `Amos Egel, Dominik Theobald, Yidenekachew Donie, Uli Lemmer, Guillaume Gomard, G: "Light scattering by oblate particles near planar interfaces: on the validity of the T-matrix approach." Optics express 24.22 (2016): 25154-25168. <https://www.osapublishing.org/DirectPDFAccess/BF1F1BA3-D9A0-E191-B22910303B4620B8_352697/oe-24-22-25154.pdf>`_

[Egel et al. 2017b]
   `Egel, A., Eremin, Y., Wriedt, T., Theobald, D., Lemmer, U., & Gomard, G. (2017). Extending the applicability of the T-matrix method to light scattering by flat particles on a substrate via truncation of sommerfeld integrals. Journal of Quantitative Spectroscopy and Radiative Transfer, 202, 279-285. <https://arxiv.org/pdf/1708.05557.pdf>`_


This book describes the Null-Field Method with Discrete Sources (NFM-DS):

[Doicu et al. 2006]
    `Doicu, Adrian, Thomas Wriedt, and Yuri A. Eremin. Light scattering by systems of particles: null-field method with discrete sources: theory and programs. Vol. 124. Springer, 2006. <http://www.springer.com/us/book/9783540336969>`_

Other publications to which we refer in this user manual:

[Wiscombe 1980]
		W.J. Wiscombe: "Improved Mie scattering algorithms", Appl. Opt. 19, 1505-1509 (1980)
		
[Neves 2012]
		Antonio A. R. Neves and Dario Pisignano: "Effect of finite terms on the truncation error of Mie series." Optics letters 37.12 (2012): 2418-2420.


Publications that use Smuthi:

[Egel et al. 2017a]
    Egel, A., Gomard, G., Kettlitz, S. W., & Lemmer, U. (2017). Accurate optical simulation of nano-particle based internal scattering layers for light outcoupling from organic light emitting diodes. Journal of Optics, 19(2), 025605.

[Theobald et al. 2017]
    Theobald, D., Egel, A., Gomard, G., & Lemmer, U. (2017). Plane-wave coupling formalism for T-matrix simulations of light scattering by nonspherical particles. Physical Review A, 96(3), 033822.

[Warren et al. 2020]
    A. Warren, M. Alkaisi and C. Moore, "Design of 2D Plasmonic Diffraction Gratings for Sensing and Super-Resolution Imaging Applications," 2020 IEEE International Instrumentation and Measurement Technology Conference (I2MTC), Dubrovnik, Croatia, 2020, pp. 1-6, doi: 10.1109/I2MTC43012.2020.9129161.

[Theobald et al. 2020]
    Theobald, D., Yu, S., Gomard, G., & Lemmer, U. (2020). Design of Selective Reflectors Utilizing Multiple Scattering by Core–Shell Nanoparticles for Color Conversion Films. ACS Photonics.
		
[Czajkowski et al. 2020]
    Czajkowski, Krzysztof M., Maria Bancerek, and Tomasz J. Antosiewicz. "Multipole analysis of substrate-supported dielectric nanoresonator arrays with T-matrix method." arXiv preprint arXiv:2006.09137 (2020).		


.. |ref NFM-DS| replace:: [Doicu et al. 2006]