The smuthi.postprocessing package
=================================

postprocessing
--------------

.. automodule:: smuthi.postprocessing
    :members:
    :undoc-members:

postprocessing.far_field
-------------------------

.. automodule:: smuthi.postprocessing.far_field
    :members:
    :undoc-members:

postprocessing.graphical_output
-------------------------------

.. automodule:: smuthi.postprocessing.graphical_output
    :members:
    :undoc-members:
		
postprocessing.internal_field

------------------------------

.. automodule:: smuthi.postprocessing.internal_field
    :members:
    :undoc-members:		

postprocessing.post_processing
------------------------------

.. automodule:: smuthi.postprocessing.post_processing
    :members:
    :undoc-members:

postprocessing.scattered_field
------------------------------

.. automodule:: smuthi.postprocessing.scattered_field
    :members:
    :undoc-members: